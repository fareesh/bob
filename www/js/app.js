

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'bob' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'bob.controllers' is found in controllers.js
angular.module('bob', ['ionic', 'bob.controllers', 'googlechart'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $stateProvider
  .state('forgot', {
	  url: '/forgot',
	  templateUrl: 'templates/forgot.html',
	  controller: 'ForgotCtrl'
  })

  .state('confirm-password', {
	  url: '/confirm-password',
	  templateUrl: 'templates/confirm-password.html',
	  controller: 'ConfirmPasswordCtrl'
  })

  .state('login-noscroll', {
	  url: '/login-noscroll',
	  templateUrl: 'templates/login-noscroll.html',
	  controller: 'LoginCtrl'
  })

  .state('login', {
	  url: '/login',
	  templateUrl: 'templates/login.html',
	  controller: 'LoginCtrl'
  })
  .state('password', {
	  url: '/password',
	  templateUrl: 'templates/password.html',
	  controller: 'PasswordCtrl'
  })
  .state('tnc', {
	  url: '/tnc',
	  templateUrl: 'templates/tnc.html',
	  controller: 'TncCtrl'
  })

  .state('mobile', {
	  url: '/mobile',
	  templateUrl: 'templates/mobile.html',
	  controller: 'MobileCtrl'
  })
  .state('app', {
	  url: '/app',
	  abstract: true,
	  templateUrl: 'templates/menu.html',
	  controller: 'AppCtrl'
  })
.state('app.account-statement', {
		url: '/account-statement',
		views: {
			'menuContent': {
				templateUrl: 'templates/account-statement.html',
				controller: 'AccountStatementCtrl'
			}
		}
	})

	.state('app.loan-account', {
		url: '/loan-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/loan-account.html',
				controller: 'LoanAccountCtrl'
			}
		}
	})
	.state('app.faq-section', {
		url: '/faq-section',
		views: {
			'menuContent': {
				templateUrl: 'templates/faq-section.html',
				controller: 'FaqSectionCtrl'
			}
		}
	})
	.state('app.mcommerce', {
		url: '/mcommerce',
		views: {
			'menuContent': {
				templateUrl: 'templates/mcommerce.html',
				controller: 'McommerceCtrl'
			}
		}
	})


	.state('app.faqs', {
		url: '/faqs',
		views: {
			'menuContent': {
				templateUrl: 'templates/faqs.html',
				controller: 'FaqsCtrl'
			}
		}
	})

	.state('app.current-account', {
		url: '/current-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/current-account.html',
				controller: 'CurrentAccountCtrl'
			}
		}
	})



	.state('app.savings-account', {
		url: '/savings-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/savings-account.html',
				controller: 'SavingsAccountCtrl'
			}
		}
	})

	.state('app.ppf-account', {
		url: '/ppf-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/ppf-account.html',
				controller: 'PpfAccountCtrl'
			}
		}
	})
	.state('app.fixed-deposit', {
		url: '/fixed-deposit',
		views: {
			'menuContent': {
				templateUrl: 'templates/fixed-deposit.html',
				controller: 'FixedDepositCtrl'
			}
		}
	})


	.state('app.withdraw', {
		url: '/withdraw',
		views: {
			'menuContent': {
				templateUrl: 'templates/withdraw.html',
				controller: 'WithdrawCtrl'
			}
		}
	})

	.state('app.contact', {
		url: '/contact',
		views: {
			'menuContent': {
				templateUrl: 'templates/contact.html',
				controller: 'ContactCtrl'
			}
		}
	})
	.state('app.change-mpin', {
		url: '/change-mpin',
		views: {
			'menuContent': {
				templateUrl: 'templates/change-mpin.html',
				controller: 'ChangeMpinCtrl'
			}
		}
	})

	.state('app.quick-bills', {
		url: '/quick-bills',
		views: {
			'menuContent': {
				templateUrl: 'templates/quick-bills.html',
				controller: 'QuickBillsCtrl'
			}
		}
	})
	.state('app.stop-cheque', {
		url: '/stop-cheque',
		views: {
			'menuContent': {
				templateUrl: 'templates/stop-cheque.html',
				controller: 'StopChequeCtrl'
			}
		}
	})
	.state('app.cheque-request', {
		url: '/cheque-request',
		views: {
			'menuContent': {
				templateUrl: 'templates/cheque-request.html',
				controller: 'ChequeRequestCtrl'
			}
		}
	})

	.state('app.cheque-book-status', {
		url: '/cheque-book-status',
		views: {
			'menuContent': {
				templateUrl: 'templates/cheque-book-status.html',
				controller: 'ChequeBookStatusCtrl'
			}
		}
	})
	.state('app.tds-statement', {
		url: '/tds-statement',
		views: {
			'menuContent': {
				templateUrl: 'templates/tds-statement.html',
				controller: 'TdsStatementCtrl'
			}
		}
	})
	.state('app.favourites', {
		url: '/favourites',
		views: {
			'menuContent': {
				templateUrl: 'templates/favourites.html',
				controller: 'FavouritesCtrl'
			}
		}
	})

	.state('app.inquire-interest', {
		url: '/inquire-interest',
		views: {
			'menuContent': {
				templateUrl: 'templates/inquire-interest.html',
				controller: 'InquireInterestCtrl'
			}
		}
	})


	.state('app.interest-certificate', {
		url: '/interest-certificate',
		views: {
			'menuContent': {
				templateUrl: 'templates/interest-certificate.html',
				controller: 'InterestCertificateCtrl'
			}
		}
	})

	.state('app.gift-card', {
		url: '/gift-card',
		views: {
			'menuContent': {
				templateUrl: 'templates/gift-card.html',
				controller: 'GiftCardCtrl'
			}
		}
	})
	.state('app.loan-schemes', {
		url: '/loan-schemes',
		views: {
			'menuContent': {
				templateUrl: 'templates/loan-schemes.html',
				controller: 'LoanSchemesCtrl'
			}
		}
	})
	.state('app.ppf-register', {
		url: '/ppf-register',
		views: {
			'menuContent': {
				templateUrl: 'templates/ppf-register.html',
				controller: 'PpfRegisterCtrl'
			}
		}
	})

	.state('app.query-account', {
		url: '/query-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/query-account.html',
				controller: 'QueryAccountCtrl'
			}
		}
	})

	.state('app.view-ppf', {
		url: '/view-ppf',
		views: {
			'menuContent': {
				templateUrl: 'templates/view-ppf.html',
				controller: 'ViewPpfCtrl'
			}
		}
	})

	.state('app.email-statement', {
		url: '/email-statement',
		views: {
			'menuContent': {
				templateUrl: 'templates/email-statement.html',
				controller: 'EmailStatementCtrl'
			}
		}
	})
	.state('app.aadhar', {
		url: '/aadhar',
		views: {
			'menuContent': {
				templateUrl: 'templates/aadhar.html',
				controller: 'AadharCtrl'
			}
		}
	})
	.state('app.twentysix-confirm', {
		url: '/twentysix-confirm',
		views: {
			'menuContent': {
				templateUrl: 'templates/twentysix-confirm.html',
				controller: 'TwentysixConfirmCtrl'
			}
		}
	})



	.state('app.twentysix', {
		url: '/twentysix',
		views: {
			'menuContent': {
				templateUrl: 'templates/twentysix.html',
				controller: 'TwentysixCtrl'
			}
		}
	})


	.state('app.cheque-status', {
		url: '/cheque-status',
		views: {
			'menuContent': {
				templateUrl: 'templates/cheque-status.html',
				controller: 'ChequeStatusCtrl'
			}
		}
	})

	.state('app.feedback', {
		url: '/feedback',
		views: {
			'menuContent': {
				templateUrl: 'templates/feedback.html',
				controller: 'FeedbackCtrl'
			}
		}
	})

	.state('app.operators', {
		url: '/operators',
		views: {
			'menuContent': {
				templateUrl: 'templates/operators.html',
				controller: 'OperatorsCtrl'
			}
		}
	})

	.state('app.quick-bill-pay', {
		url: '/quick-bill-pay',
		views: {
			'menuContent': {
				templateUrl: 'templates/quick-bill-pay.html',
				controller: 'QuickBillPayCtrl'
			}
		}
	})


	.state('app.bill-pay', {
		url: '/bill-pay',
		views: {
			'menuContent': {
				templateUrl: 'templates/bill-pay.html',
				controller: 'BillPayCtrl'
			}
		}
	})

	.state('app.data-recharge', {
		url: '/data-recharge',
		views: {
			'menuContent': {
				templateUrl: 'templates/data-recharge.html',
				controller: 'DataRechargeCtrl'
			}
		}
	})


	.state('app.dth-recharge', {
		url: '/dth-recharge',
		views: {
			'menuContent': {
				templateUrl: 'templates/dth-recharge.html',
				controller: 'DthRechargeCtrl'
			}
		}
	})



	.state('app.mobile-recharge', {
		url: '/mobile-recharge',
		views: {
			'menuContent': {
				templateUrl: 'templates/mobile-recharge.html',
				controller: 'MobileRechargeCtrl'
			}
		}
	})
	.state('app.transaction-details', {
		url: '/transaction-details',
		views: {
			'menuContent': {
				templateUrl: 'templates/transaction-details.html',
				controller: 'TransactionDetailsCtrl'
			}
		}
	})

	.state('app.transaction-history', {
		url: '/transaction-history',
		views: {
			'menuContent': {
				templateUrl: 'templates/transaction-history.html',
				controller: 'TransactionHistoryCtrl'
			}
		}
	})

	.state('app.service-requests', {
		url: '/service-requests',
		views: {
			'menuContent': {
				templateUrl: 'templates/service-requests.html',
				controller: 'ServiceRequestsCtrl'
			}
		}
	})
	.state('app.payment-type-menu', {
		url: '/payment-type-menu',
		views: {
			'menuContent': {
				templateUrl: 'templates/payment-type-menu.html',
				controller: 'PaymentTypeMenuCtrl'
			}
		}
	})

	.state('app.deregister-menu', {
		url: '/deregister-menu',
		views: {
			'menuContent': {
				templateUrl: 'templates/deregister-menu.html',
				controller: 'DeregisterMenuCtrl'
			}
		}
	})
	.state('app.cancel-mmid', {
		url: '/cancel-mmid',
		views: {
			'menuContent': {
				templateUrl: 'templates/cancel-mmid.html',
				controller: 'CancelMmidCtrl'
			}
		}
	})

	.state('app.block-card', {
		url: '/block-card',
		views: {
			'menuContent': {
				templateUrl: 'templates/block-card.html',
				controller: 'BlockCardCtrl'
			}
		}
	})

	.state('app.deregister-payment', {
		url: '/deregister-payment/:type',
		views: {
			'menuContent': {
				templateUrl: 'templates/deregister-payment.html',
				controller: 'DeregisterPaymentCtrl'
			}
		}
	})

	.state('app.deregister', {
		url: '/deregister/:type',
		views: {
			'menuContent': {
				templateUrl: 'templates/deregister.html',
				controller: 'DeregisterCtrl'
			}
		}
	})


	.state('app.imps-aadhaar', {
		url: '/imps-aadhaar',
		views: {
			'menuContent': {
				templateUrl: 'templates/imps-aadhaar.html',
				controller: 'ImpsAadhaarCtrl'
			}
		}
	})


	.state('app.imps-merchant', {
		url: '/imps-merchant',
		views: {
			'menuContent': {
				templateUrl: 'templates/imps-merchant.html',
				controller: 'ImpsMerchantCtrl'
			}
		}
	})


	.state('app.imps-to-mmid', {
		url: '/imps-to-mmid',
		views: {
			'menuContent': {
				templateUrl: 'templates/imps-to-mmid.html',
				controller: 'ImpsToMmidCtrl'
			}
		}
	})


	.state('app.neft-to-account', {
		url: '/neft-to-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/neft-to-account.html',
				controller: 'NeftToAccountCtrl'
			}
		}
	})

	.state('app.beneficiary-neft-to-account', {
		url: '/beneficiary-neft-to-account',
		views: {
			'menuContent': {
				templateUrl: 'templates/beneficiary-neft-to-account.html',
				controller: 'BeneficiaryNeftToAccountCtrl'
			}
		}
	})
	.state('app.beneficiary-registration', {
		url: '/beneficiary-registration',
		views: {
			'menuContent': {
				templateUrl: 'templates/beneficiary-registration.html',
				controller: 'BeneficiaryRegistrationCtrl'
			}
		}
	})

	.state('app.neft-beneficiary', {
		url: '/neft-beneficiary',
		views: {
			'menuContent': {
				templateUrl: 'templates/neft-beneficiary.html',
				controller: 'NeftBeneficiaryCtrl'
			}
		}
	})
	.state('app.merchant-transfer', {
		url: '/merchant-transfer',
		views: {
			'menuContent': {
				templateUrl: 'templates/merchant-transfer.html',
				controller: 'MerchantTransferCtrl'
			}
		}
	})

	.state('app.aadhaar-transfer', {
		url: '/aadhaar-transfer',
		views: {
			'menuContent': {
				templateUrl: 'templates/aadhaar-transfer.html',
				controller: 'AadhaarTransferCtrl'
			}
		}
	})


	.state('app.imps-transfer', {
		url: '/imps-transfer',
		views: {
			'menuContent': {
				templateUrl: 'templates/imps-transfer.html',
				controller: 'ImpsTransferCtrl'
			}
		}
	})
	.state('app.retrieve-mmid', {
		url: '/retrieve-mmid',
		views: {
			'menuContent': {
				templateUrl: 'templates/retrieve-mmid.html',
				controller: 'RetrieveMmidCtrl'
			}
		}
	})

	.state('app.ppf-deregister-menu', {
		url: '/ppf-deregister-menu',
		views: {
			'menuContent': {
				templateUrl: 'templates/ppf-deregister-menu.html',
				controller: 'PpfDeregisterMenuCtrl'
			}
		}
	})

	.state('app.credit-ppf', {
		url: '/credit-ppf',
		views: {
			'menuContent': {
				templateUrl: 'templates/credit-ppf.html',
				controller: 'CreditPpfCtrl'
			}
		}
	})

	.state('app.ifsc-transfer', {
		url: '/ifsc-transfer',
		views: {
			'menuContent': {
				templateUrl: 'templates/ifsc-transfer.html',
				controller: 'IfscTransferCtrl'
			}
		}
	})

	.state('app.mobile-transfer', {
		url: '/mobile-transfer',
		views: {
			'menuContent': {
				templateUrl: 'templates/mobile-transfer.html',
				controller: 'MobileTransferCtrl'
			}
		}
	})

	.state('app.self-linked-accounts', {
		url: '/self-linked-accounts',
		views: {
			'menuContent': {
				templateUrl: 'templates/self-linked-accounts.html',
				controller: 'SelfLinkedAccountsCtrl'
			}
		}
	})

	.state('app.transfer-to-third-party', {
		url: '/transfer-to-third-party',
		views: {
			'menuContent': {
				templateUrl: 'templates/transfer-to-third-party.html',
				controller: 'TransferToThirdPartyCtrl'
			}
		}
	})
	.state('app.imps-options', {
		url: '/imps-options',
		views: {
			'menuContent': {
				templateUrl: 'templates/imps-options.html',
				controller: 'ImpsOptionsCtrl'
			}
		}
	})


	.state('app.fund-transfer', {
		url: '/fund-transfer',
		views: {
			'menuContent': {
				templateUrl: 'templates/fund-transfer.html',
				controller: 'FundTransferCtrl'
			}
		}
	})

	.state('app.bills', {
		url: '/bills',
		views: {
			'menuContent': {
				templateUrl: 'templates/bills.html',
				controller: 'BillsCtrl'
			}
		}
	})

	.state('app.confirm-neft', {
		url: '/confirm-neft',
		views: {
			'menuContent': {
				templateUrl: 'templates/confirm-neft.html',
				controller: 'ConfirmNeftCtrl'
			}
		}
	})

	.state('app.neft', {
		url: '/neft',
		views: {
			'menuContent': {
				templateUrl: 'templates/neft.html',
				controller: 'NeftCtrl'
			}
		}
	})

	.state('app.accounts', {
		url: '/accounts',
		views: {
			'menuContent': {
				templateUrl: 'templates/accounts.html',
				controller: 'AccountsCtrl'
			}
		}
	})

	.state('app.details', {
		url: '/details',
		views: {
			'menuContent': {
				templateUrl: 'templates/account-details.html',
				controller: 'DetailsCtrl'
			}
		}
	})


	.state('app.term-deposit', {
		url: '/term-deposit',
		views: {
			'menuContent': {
				templateUrl: 'templates/term-deposit.html',
				controller: 'TermDepositCtrl'
			}
		}
	})
	.state('app.beneficiary', {
		url: '/beneficiary',
		views: {
			'menuContent': {
				templateUrl: 'templates/beneficiary.html',
				controller: 'BeneficiaryCtrl'
			}
		}
	})
	.state('app.toggle-accounts', {
		url: '/toggle-accounts',
		views: {
			'menuContent': {
				templateUrl: 'templates/toggle-accounts.html',
				controller: 'ToggleCtrl'
			}
		}
	})


	.state('app.summary', {
		url: '/summary',
		views: {
			'menuContent': {
				templateUrl: 'templates/summary.html',
				controller: 'SummaryCtrl'
			}
		}
	})

	.state('app.home-noscroll', {
		url: '/home-noscroll',
		views: {
			'menuContent': {
				templateUrl: 'templates/home-noscroll.html',
				controller: 'HomeCtrl'
			}
		}
	})
	.state('app.mini-statement', {
		url: '/mini-statement',
		views: {
			'menuContent': {
				templateUrl: 'templates/mini-statement.html',
				controller: 'MiniStatementCtrl'
			}
		}
	})
	.state('app.home', {
		url: '/home',
		views: {
			'menuContent': {
				templateUrl: 'templates/home.html',
				controller: 'HomeCtrl'
			}
		}
	});
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
  $ionicConfigProvider.navBar.alignTitle('left');

});
