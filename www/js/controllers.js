angular.module('bob.controllers', [])
.controller('ChqStatCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading, $sce){

	$scope.data = {};
	$scope.data.btNam1 = "test";
})

.controller('LoanAccountCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading, $sce){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.isNumber = function(x){
		return typeof x == "number";
	};
	$scope.data = {
		customer_id: {label: "Customer ID", value: 123456},
		scheme: {label: "Scheme", value: "Home"},
		account_number: {label: "Account Number", value: 12345674574},
		branch_name: {label: "Branch name", value: "BKC Mumbai"},
		open_date: {label: "Account opening date", value: "25-5-2015"},
		nominee_details: {label: "Nominee Details", value: $sce.trustAsHtml("<button class='bob-options button icon-right ion-chevron-right'>Kartik Aradhya</button>")},
		email: {label: "Email", value: "test@test.com"},
		pan: {label: "PAN card number", value: "AHYT91781"},
		balance: {label: "Available balance", value: 124124},
		customer_address: {label: "Customer Address", value: "#49, 7th cross, 11th main, SLV colony, JP Nagar 7th Phase, Bangalore - 560078", multiline: true},
		interest: {label: "Interest rate", value: "6%"},
		period: {label: "Period/Tenure", value: "12 months"},
		num_installments: {label: "No. of Installments", value: 9},
		due_date: {label: "Next due date", value: "21-03-2017"},
		installments: {label: "Installment amount", value: 1241},

	};
})
.controller('CurrentAccountCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading, $sce){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.isNumber = function(x){
		return typeof x == "number";
	};
	$scope.data = {
		customer_id: {label: "Customer ID", value: 123456},
		account_number: {label: "Account Number", value: 12345674574},
		branch_name: {label: "Branch name", value: "BKC Mumbai"},
		debit_cards: {label: "Debit Cards", value: ["*******","*******","*******"].join("<br/>")},
		open_date: {label: "Account opening date", value: "25-5-2015"},
		nominee_details: {label: "Nominee Details", value: $sce.trustAsHtml("<button class='bob-options button icon-right ion-chevron-right'>Kartik Aradhya</button>")},
		registered_user: {label: "Registered Internet<br/>banking user",value: "Yes"},
		email: {label: "Email", value: "test@test.com"},
		pan: {label: "PAN/TIN number", value: "AHYT91781"},
		aadhaar: {label: "Aadhaar card number", value: "123456789012"},
		balance: {label: "Available balance", value: 124124},
		ledger_balance: {label: "Ledger balance", value: 124124},
		debit_amount: {label: "Debit amount in queue", value: 124124},
		customer_address: {label: "Customer Address", value: "#49, 7th cross, 11th main, SLV colony, JP Nagar 7th Phase, Bangalore - 560078", multiline: true},
	};
})

.controller('SavingsAccountCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading, $sce){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.isNumber = function(x){
		return typeof x == "number";
	};
	$scope.data = {
		customer_id: {label: "Customer ID", value: 123456},
		scheme: {label: "Scheme", value: "Home"},
		account_number: {label: "Account Number", value: 12345674574},
		branch_name: {label: "Branch name", value: "BKC Mumbai"},
		debit_cards: {label: "Debit Cards", value: ["*******","*******","*******"].join("<br/>")},
		open_date: {label: "Account opening date", value: "25-5-2015"},
		nominee_details: {label: "Nominee Details", value: $sce.trustAsHtml("<button class='bob-options button icon-right ion-chevron-right'>Kartik Aradhya</button>")},
		registered_user: {label: "Registered Internet<br/>banking user",value: "Yes"},
		email: {label: "Email", value: "test@test.com"},
		pan: {label: "PAN card number", value: "AHYT91781"},
		aadhaar: {label: "Aadhaar card number", value: "123456789012"},
		balance: {label: "Available balance", value: 124124},
		ledger_balance: {label: "Ledger balance", value: 124124},
		debit_amount: {label: "Debit amount in queue", value: 124124},
		customer_address: {label: "Customer Address", value: "#49, 7th cross, 11th main, SLV colony, JP Nagar 7th Phase, Bangalore - 560078", multiline: true},
	};
})

.controller('PpfAccountCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading, $sce){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.isNumber = function(x){
		return typeof x == "number";
	};
	$scope.data = {
		customer_id: {label: "Customer ID", value: 123456},
		scheme: {label: "Scheme", value: "Home"},
		account_number: {label: "Account Number", value: 12345674574},
		branch_name: {label: "Branch name", value: "BKC Mumbai"},
		open_date: {label: "Account opening date", value: "25-5-2015"},
		nominee_details: {label: "Nominee Details", value: $sce.trustAsHtml("<button class='bob-options button icon-right ion-chevron-right'>Kartik Aradhya</button>")},
		email: {label: "Email", value: "test@test.com"},
		pan: {label: "PAN", value: "AHYT91781"},
		balance: {label: "Available balance", value: 124124},
		customer_address: {label: "Customer Address", value: "#49, 7th cross, 11th main, SLV colony, JP Nagar 7th Phase, Bangalore - 560078", multiline: true},
	};
})

.controller('FixedDepositCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading, $sce){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.isNumber = function(x){
		return typeof x == "number";
	};
	$scope.data = {
		customer_id: {label: "Customer ID", value: 123456},
		scheme: {label: "Scheme", value: "Home"},
		account_number: {label: "Account Number", value: 12345674574},
		branch_name: {label: "Branch name", value: "BKC Mumbai"},
		open_date: {label: "Account opening date", value: "25-5-2015"},
		nominee_details: {label: "Nominee Details", value: $sce.trustAsHtml("<button class='bob-options button icon-right ion-chevron-right'>Kartik Aradhya</button>")},
		email: {label: "Email", value: "test@test.com"},
		pan: {label: "PAN", value: "AHYT91781"},
		balance: {label: "Available balance", value: 124124},
		maturity_amount: {label: "Maturity Amount", value: 15125},
		customer_address: {label: "Customer Address", value: "#49, 7th cross, 11th main, SLV colony, JP Nagar 7th Phase, Bangalore - 560078", multiline: true},
		interest: {label: "Interest rate", value: "6%"},
		period: {label: "Period/Tenure", value: "12 months"},
		num_installments: {label: "No. of Installments for RD", value: 9},
		due_date: {label: "Next due date for RD", value: "21-03-2017"},
		installments: {label: "Installment amount for RD", value: 1241},
	};
})


.controller('ChangeMpinCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
			title: 'Your MPIN has been changed.',
			buttons: [{text: "OK", type: "button-gradient"}]
		});
	};

})
.controller('CancelMmidCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('BlockCardCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.cards = ["125125125125","125125125125125","512512512512"];
})
.controller('AccountStatementCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup){


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.accList = [
		{"AC_TYPE": "SBA"},
		{"AC_TYPE": "CAA"},
		{"AC_TYPE": "ODA"},
	];
	$scope.data.savings = {name: "Krupankaya Vijay", account_number: "1241241241", balance: "12352"};
	$scope.data.current = {name: "Krupankaya Vijay", account_number: "1241241241", balance: "12352"};
	$scope.data.overdraft = {name: "Krupankaya Vijay", account_number: "1241241241", balance: "12352"};
	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
			templateUrl: 'templates/select-account.html',
			buttons: [{text: "OK", type: "button-gradient"}],
			cssClass: 'wide-popup'
		});
	};

})



.controller('FavouritesCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('InquireInterestCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('TwentysixConfirmCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('TwentysixCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('TdsStatementCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('InterestCertificateCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('GiftCardCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('LoanSchemesCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('PpfRegisterCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('QueryAccountCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('ViewPpfCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('EmailStatementCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('AadharCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('FaqSectionCtrl', function($scope, $ionicModal, $timeout, $state, $sce){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.questions = [{question: "How to register M-Connect?", answer: $sce.trustAsHtml("Base Branch / ATM / Net Banking.<br/><ul><li>Simple form to be submitted to base branch</li><li>Swipe Card &gt; M-Connect &gt; Registration &gt; Enter mobile number &gt; Successful!</li></ul>")}];
})


.controller('FaqsCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('TransactionHistoryCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.transaction-details');
	};

	$scope.transactions = ["Mobile Recharge - Rs. 2500", "NEFT - Rs. 17000"];

})
.controller('TransactionDetailsCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.details = [
		{label: "Service", value: "IMPS"},
		{label: "Beneficiary", value: "Test"},
		{label: "Account Number", value: "1212512"},
		{label: "Amount", value: "4124"},
		{label: "Transaction ID", value: "1521512515"},
		{label: "Date & Time", value: new Date()},
	];

})
.controller('ServiceRequestsCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.viewAcc = function(type) {
		console.log(type);
		if(type === "CBQ") {
			$state.go("app.cheque-request");	
		}else if (type === "tsCB" ) {
			$state.go("app.cheque-book-status");	
		} else if (type === "SIC") {
			$state.go("app.cheque-status");	
		}else if (type === "STC") {
			$state.go("app.stop-cheque");	
		}else if (type === "RVC") {
			console.log("Clicked revoke cheque");
		}
	}
})

.controller('ChequeRequestCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('ChequeBookStatusCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('ChequeStatusCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('StopChequeCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.btNam1 = "test";

})


.controller('ConfirmPasswordCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})



.controller('ForgotCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('LoginCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.options = {autoplay: 2500, loop: true, speed: 600};

	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.viewNewUser = function(){
		$state.go('mobile');
	};

	$scope.viewForgotPwd = function(){
		$state.go('forgot');
	};


	$scope.validateCredentials = function(){
		$state.go('app.home');
	};
})
.controller('PasswordCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
			template: 'Request processed successfully.',
			buttons: [{text: "OK", type: "button-gradient"}]
		});

		alertPopup.then(function(res) {
			$state.go('tnc');
		});
	};

})
.controller('NeftBeneficiaryCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('AadhaarTransferCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})
.controller('MerchantTransferCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('ImpsTransferCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

	$scope.data.btAcntNum = 112412412;

})
.controller('RetrieveMmidCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.mmid = "123515125125";
})

.controller('PpfDeregisterMenuCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.ppfs = ["124124124124", "12412412414"];
})

.controller('CreditPpfCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})


.controller('IfscTransferCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('MobileTransferCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('SelfLinkedAccountsCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('TransferToThirdPartyCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('ImpsOptionsCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.viewCancelMMID = function(){
		$state.go('app.cancel-mmid')
	};
	
	$scope.viewRetrieveMMID = function(){
		$state.go('app.retrieve-mmid');
	};

	$scope.viewNextPage = function(type){
		switch(type){
			case "impsaccount":
				$state.go('app.ifsc-transfer');
				break;
			case "impsmobile":
				$state.go('app.imps-transfer');
				break;
			case "aadhar":
				$state.go('app.imps-aadhaar');
				break;
			case "merchant":
				$state.go('app.imps-merchant');
				break;
		}
	};
})

.controller('FundTransferCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.viewNeft = function(){
		$state.go('app.neft');
	};

	$scope.viewAccs = function(acc_type) {
		if(acc_type === "sla"){
			$state.go("app.self-linked-accounts");
		}else if (acc_type === "tpa") {
			$state.go("app.transfer-to-third-party");
		}
	}

	$scope.viewIMPS = function(){
		$state.go("app.imps-options");	
	}

	$scope.viewNeft = function() {
		$state.go("app.neft");	
	}
})
.controller('DeregisterCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){

	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('ImpsAadhaarCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.data = {};
	$scope.step = 0;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('ImpsMerchantCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.data = {};
	$scope.step = 0;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

	$scope.data.btAcntNum = "12412412";


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})


.controller('ImpsToMmidCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.data = {};
	$scope.step = 0;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('NeftToAccountCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.data = {};
	$scope.data.bank_name = "TEST TEST TEST TEST TEST TEST";
	$scope.step = 0;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};


	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
			templateUrl: 'templates/transaction-popup.html',
			buttons: [{text: "OK", type: "button-gradient"}],
			cssClass: 'wide-popup'
		});
	};


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('BeneficiaryNeftToAccountCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.data = {};
	$scope.data.bank_name = "TEST TEST TEST TEST TEST TEST";
	$scope.step = 0;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};


	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
			templateUrl: 'templates/transaction-popup.html',
			buttons: [{text: "OK", type: "button-gradient"}],
			cssClass: 'wide-popup'
		});
	};


	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('BeneficiaryManagementCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('BeneficiaryRegistrationCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('McommerceCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

	$scope.viewQuickBillPay = function() {
		$state.go("app.quick-bills");
	}

	$scope.viewBenf = function() {
		$state.go("app.beneficiary");
	}
})
.controller('FeedbackCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('BeneficiaryCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('ToggleCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.showMpin = function(){
		var otpPopup = $ionicPopup.prompt({
			title: "Enter MPIN",
			scope: $scope,
			buttons: [{text: "OK", type:  "button-gradient icon-left ion-checkmark"}, {text: "Cancel", type: "button-gradient icon-left ion-close"}]
		});
	};

	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('DeregisterPaymentCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.payment_type = "Mobile Recharge";
	$scope.data.beneficiaries = [
		{name: "Ms. Krupanka Vijay", account_number: "329100091237"},
		{name: "Mr. Shashank", account_number: "329100091237"}
	];
})

.controller('DeregisterCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {};
	$scope.data.payment_type = "NEFT";
	$scope.data.beneficiaries = [
		{name: "Ms. Krupanka Vijay", account_number: "329100091237 [SA]"},
		{name: "Mr. Shashank", account_number: "329100091237 [SA]"}
	];
})
.controller('PaymentTypeMenuCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.payment_types = [{label: "Mobile Recharge", url: "/#/app/deregister/neft"}, {label: "DTH", url: "/#/app/deregister/aadhaar"}, {label: "Data card", url: "/#/app/deregister/merchant"}];
})


.controller('DeregisterMenuCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.payment_types = [{label: "NEFT", url: "/#/app/deregister/neft"}, {label: "IMPS-Account", url: "/#/app/deregister/account"}, {label: "IMPS-Mobile", url: "/#/app/deregister/aadhaar"}, {label: "IMPS-Merchant", url: "/#/app/deregister/merchant"}];
})

.controller('OperatorsCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('DthRechargeCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.step = 0;
	$scope.data = {};
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})
.controller('DataRechargeCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.data = {};
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('NeftCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.data = {};
	$scope.data.available_balance = 9999999;
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};

})


.controller('MobileRechargeCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.data = {};
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})

.controller('QuickBillPayCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.step = 0;
	$scope.data = {};
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('WithdrawCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.step = 0;
	$scope.data = {};
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('BillPayCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.step = 0;
	$scope.data = {};
	$scope.toggleStep = function(){
		$scope.step+=1;
		$scope.step%=2;
	};

})

.controller('DetailsCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
	$scope.data = {
		customer_id: {label: "Customer ID", value: 123456},
		account_number: {label: "Account Number", value: 12345674574},
		branch_sol_id: {label: "Branch Sol ID", value: "AW1241"},
		branch_code: {label: "Branch Code", value: "124124"},
		open_date: {label: "Account opening date", value: "25-5-2015"},
		nominee_details: {label: "Nominee Details", value: "Karthik Aradhya"},
		balance: {label: "Available balance", value: 124124},
		ledger_balance: {label: "Ledger Balance", value: 15125},
		debit_queue: {label: "Debit Amount in queue", value: 1241},
		maturity: {label: "Maturity amount for only<br/> term deposit account", value: 1241},
		installments: {label: "Installments for term deposit<br/> account &amp; loan account", value: 1241},
		interest: {label: "Interest rate for term deposit<br/> account &amp; loan account", value: 1241},
		period: {label: "Period for term deposit<br/> account &amp; loan account", value: "12|24"},
		num_installments: {label: "No. of installments for both<br/>Term deposit (recurring)<br/>", value: 1},
		loan_installments: {label: "&amp; Loan Account", value: 11},
		due_date: {label: "Next due date for both<br/>Term deposit (recurring)", value: "1st sep"},
		loan_due_date: {label: "&amp; Loan Account", value: "1st  Aug"},

		amount_installments: {label: "Installment amount for both<br/>Term deposit (recurring)<br/>", value: 1000},
		loan_amount_installments: {label: "&amp; Loan Account", value: 100}
	};
})


.controller('ContactCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('TermDepositCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.data = {};
	$scope.data.name = "Krupanka Vijay";
	$scope.data.account_number = "29040100011118";
	$scope.data.available_balance = "1083570";
	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})


.controller('TncCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent eu libero bibendum, tempor arcu id, dignissim lectus. Aliquam erat volutpat. Suspendisse potenti. Integer vitae neque ex. Suspendisse potenti. Nam ut turpis ullamcorper lacus lobortis sodales. Sed auctor augue in ex porttitor consectetur. Quisque eget arcu risus. Curabitur pellentesque luctus nisi, ut posuere mauris egestas in. Suspendisse placerat maximus rhoncus. Curabitur lorem mauris, rutrum non erat id, volutpat fermentum turpis.";

	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('app.home');
	};
})
.controller('MobileCtrl', function($scope, $ionicModal, $timeout, $state, $ionicPopup, $ionicLoading){
	$scope.showValidating = function(){
		$ionicLoading.show({
		});
	};
	$scope.showOTP = function(){
		var otpPopup = $ionicPopup.prompt({
			title: "OTP",
			scope: $scope,
			buttons: [{text: "OK", type:  "button-gradient icon-left ion-checkmark"}, {text: "Cancel", type: "button-gradient icon-left ion-close"}]
		});
	};
	$scope.showAlert = function() {
		var alertPopup = $ionicPopup.alert({
			template: 'OTP sent to your mobile. Please use that to register further.',
			buttons: [{text: "OK", type: "button-gradient"}]
		});

		alertPopup.then(function(res) {
			$scope.nextScreen();
		});
	};

	$scope.nextScreen = function(){
		console.log("Going");
		$state.go('password');
	};
})

.controller('SummaryCtrl', function($scope, $ionicModal, $timeout){
	$scope.data = {};
	$scope.data.balance = "10,81,500";
	$scope.data.account_number = 123456789012;
	$scope.myChartObject = {};
    
    $scope.myChartObject.type = "PieChart";

	$scope.data.transactions = [
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "credit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "credit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000},
		{id: 1, date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000},
		{date: "01/01/2015", description: "PayPal", transaction_type: "debit", balance: "10,79,500", amount: 1000}


	];

	$scope.rows = $scope.data.transactions.map(function(k,v){
		return {c: [{v: k.transaction_type}, {v: k.amount}]};
	});

	console.log($scope.rows);

    
    $scope.myChartObject.data = {"cols": [
        {id: "t", label: "Type", type: "string"},
        {id: "s", label: "Amount", type: "number"}
    ], "rows": $scope.rows};

	$scope.clickEvent = function(selectedItem){
		console.log(selectedItem);
		return false;
	};

    $scope.myChartObject.options = {
		is3D: true,
		slices: {  1: {offset: 0.2, 2: {offset: 0.5}, 3: {offset: 0.9}},
		},
		legend: {position: 'none'},
		colors: ["red","green"],
		'tooltip' : {
			trigger: 'none'
		}
    };

})
.controller('AccountsCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.data = {};
	$scope.viewAccs = function(acc_name) {
		if (acc_name === "ta") {
			$state.go('app.account-statement');
		}else if (acc_name === "td") {
			$state.go('app.term-deposit');
		}else if (acc_name === "la") {
			$state.go('app.loan-account');
		}else if (acc_name === "ppf") {
			$state.go('app.ppf-account');
		}
	}
})
.controller('QuickBillsCtrl', function($scope, $ionicModal, $timeout, $state){
	$scope.data = {};
})

.controller('MiniStatementCtrl', function($scope, $ionicPopup, $ionicModal, $timeout, $state){
	$scope.showAlert = function() {
	console.log($scope.data);
		var alertPopup = $ionicPopup.alert({
			templateUrl: 'templates/mini-statement-popup.html',
			buttons: [{text: "OK", type: "button-gradient"}]
		});

		alertPopup.then(function(res) {
			console.log('You are sure');
		});
	};
})

.controller('HomeCtrl', function($scope, $ionicModal, $timeout, $state, $ionicSideMenuDelegate){
	$scope.data = {};
	$scope.data.user = {};
	$scope.data.user.salutation = "Ms. ";
	$scope.data.user.first_name = "Amisha";
	$scope.data.user.customer_name = "Ms. Amisha Kumari";
	$scope.data.user.last_name = "Kumari";
	$scope.data.user.last_login = "June 16th, 09:00 AM";


	$scope.viewAcc = function(){
		$state.go('app.accounts');
	};

	$scope.viewFundTrans = function(){
		$state.go('app.fund-transfer');
	};

	$scope.viewFaqs = function(){
		$state.go('app.faqs');
	};

	$scope.viewContactUs = function(){
		$state.go('app.contact');
	};
	$scope.viewReqServices = function(){
		$state.go('app.service-requests');
	};


})
.controller('AppCtrl', function($scope, $ionicModal, $timeout, $state, $rootScope) {
	$scope.viewCmplntnFeedback = function() {
		$state.go('app.feedback');
	};
	$scope.viewFTransfer = function(){
		$state.go('app.fund-transfer');
	};

	$scope.viewMcomm = function(){
		$state.go('app.mcommerce');
	};

	$scope.viewFavourites = function(){
		$state.go('app.favourites');
	};
	
	$scope.viewHome = function(){
		$state.go('app.home');
	};

	$scope.$on('$stateChangeSuccess', function(evt, toState, toParams, fromState, fromParams) {                  
		$rootScope.currentState = toState.name;
	});
	$scope.viewWithDrwl= function(){
		$state.go('app.withdraw');
	};
	$scope.viewTnC= function(){
		$state.go('tnc');
	};
})


